const http = require('http');
const fs = require('fs'); //File system?

const hostname = '127.0.0.1';
const port = 3000;

fs.readFile('index.html', (err, html) => {
    if (err) throw err;

    /**
     * req = request
     * res = response
     */
    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-type', 'text/html');
        res.write(html);
        res.end();
    });

    /**
     *the listen method takes in a port, hostname and callback function
     */
    server.listen(port, hostname, () => {
        console.log(`Server started on port ${port}`);
    });
});
