/*
    Source: https://www.youtube.com/watch?v=pWbMrx5rVBE
    Title: MongoDB In 30 Minutes
    
    Video Description:
    Traversy Media
    Published on Sep 4, 2016

    In this video we will dive into the MongoDB NoSQL database and look at the
    fundamentals and the syntax to create, read, update and delete documents/data
*/

//Start up mongo DB
mongod

//Start up a mongo db shell
mongo --host localhost:27017

//Create and use this database
use mycustomers

//Show the db in use
db

//Show all dbs available
show dbs

//Create a user for this database
//See documentation https://docs.mongodb.com/manual/reference/method/db.createUser/
db.createUser({
     user: "buki",
     pwd: "1234",
     roles: [ "readWrite", "dbAdmin" ]
 });

//Create a collection called customers
db.createCollection('customers');

//View all collections
show collections

//Insert an object into a collection
db.customers.insert({first_name: "John", last_name: "Does"})

//Insert multiple customers into a collection and even add a new field
db.customers.insert([
     {first_name: "Bola", last_name: "Olu"},
     {first_name: "Jane", last_name: "Doe"},
     {first_name: "Buki", last_name: "A", gender: "female"},
 ])

 //View the contents of your collection
 db.customers.find()

 //List collection contents in a formatted way
 db.customers.find().pretty()

 //Update an entry by adding a gender
 /*
 Match the first name, for instance, by typing
 db.customers.update({first_name: "John"}) which will update all entries with the first name, John
 It's probably better to use the auto created unique ID

The next parameter will be the object we want to replace it with:
*/
db.customers.update({first_name: "John"}, {first_name: "John", last_name: "Doe", gender: "Male"})

/*Update an entry by modifying only one property.
This updates the first matching entry found*/
db.customers.update({first_name: "Bola"}, {$set:{gender: "female"}})

/*
    Incrememt a numerical value using $inc
    Add an age, then pass the number by which you want to Incrememt. By the end of this, Bola will have an age of 20
*/
db.customers.update({first_name: "Bola"}, {$set: {age: 13}})
db.customers.update({first_name: "Bola"}, {$inc: {age: 7}})

//Remove a field using unset (the value of age doesn't matter in this example)
db.customers.update({first_name: "Bola"}, {$unset:{age:0}})

/*
    What about extra options to our query?
    Say we try to update something that doesn't exist and we want to add the entry if it's not found
*/
db.customers.update(
    {first_name: "Blossom"},
    {first_name: "Blossom", last_name:"Powerpuff", age: 10, gender: "super"},
    {upsert: true});

//Rename a property
db.customers.update({first_name: "Blossom"}, {$rename:{"gender": "sex"}})

//Remove all documents that match a criterion
db.customers.remove({first_name: "John"})

// Extra options to our query: only remove the first one found
db.customers.remove({first_name: "Bola"}, {justOne: true})

//To find a specific customer (and not do anything to it but return it)
db.customers.find({first_name:"Blossom"})

//Finding with multiple queries e.g. using an "or"
db.customers.find({$or:[{first_name: "Buki"}, {first_name: "Blossom"}]})

/*
    Say we wanted to find everyone with an age under 40 and pretty print our result
    $lt means "less than"
    Conversely, $gt would be "greater than"
    $lte - less than or equal to
    $gte - greater than or equal to
*/
db.customers.find({age: {$lt:40}}).pretty();

//Query the db using a nested object property. Add some quotes around your key and nest away.
//I'll add some new stuff first
db.customers.insert([
    {first_name: "Buki", last_name: "O", gender: "female", address: {street: "1234", city: "Abuja", country: "Nigeria"}},
    {first_name: "Bayo", last_name: "E", gender: "male", address: {street: "8093", city: "Abuja", country: "Nigeria"}},
    {first_name: "Ryan", last_name: "M", gender: "male", address: {street: "5567", city: "Dubai", country: "UAE"}},
])
//Then query my collection
db.customers.find({"address.city": "Abuja"})

//For searching within an array, you only have to write the element of the array you are searching for
db.customers.insert(
    {
        first_name: "Kemi",
        last_name: "A",
        gender: "female",
        address: {street: "1234", city: "Abuja", country: "Nigeria"},
        siblings: ["Buki", "Banjo"]
    }
)
db.customers.find({siblings: "Banjo"})

//Let's find by sorting last name (1 for ascending, -1 for descending)
db.customers.find().sort({last_name: 1})
db.customers.find().sort({last_name: -1})

//To count documents (which can also be queried)
db.customers.find().count()
db.customers.find({gender: "male"}).count()

//Fnd everyone but set a limit (and let's combine a few queries we've learned so far)
db.customers.find().limit(2).sort({last_name: 1})

//Iterate using forEach just like in regular javascript
db.customers.find().forEach((customer) =>{print(`Customer name: ${customer.first_name}`)})
