const express = require('express')
const router = express.Router()
const path = require('path');
const User = require('../models/user')
var formidable = require('formidable')
var fs = require('fs')

//const passport = require('passport')
//const jwt = require('jsonwebtoken')
const config = require('../config/database')


/*
	Sources:
	https://www.w3schools.com/nodejs/nodejs_uploadfiles.asp
	https://www.npmjs.com/package/formidable
	https://stackoverflow.com/a/46184234
	https://stackoverflow.com/a/8774566
	https://stackoverflow.com/a/26815894
*/
router.post('/', (req, res, next) => {
	const form = new formidable.IncomingForm({
		encoding: 'utf-8',
		multiples: false,
		keepExtensions: true
	})

	form.once('error', console.log)

	form.parse(req, (err, fields, files) => {
		if (err) {
			res.json({
				error: err,
				success: false,
				msg: 'Error parsing file',
			})
		}

		const oldpath = files.file.path
		const uploadDirectory = 'uploads'
		const newPath = `${uploadDirectory}/${files.file.name}`
		const publicPath = encodeURI(`${config.baseurl}/uploads/${files.file.name}`)

		if (!fs.existsSync(uploadDirectory)) {
			console.log("Creating uploads directory")
			fs.mkdirSync(uploadDirectory, (err) => {
				if (err) console.log("Error creating upload directory", err)
			});
		}

		fs.rename(oldpath, newPath, (err) => {
			if (err) {
				console.log("Error renaming file: ", err)
				return res.json({
					error: err,
					success: false,
					msg: 'Error renaming and saving file',
				})
			}
			res.json({
				success: true,
				msg: 'file uploaded',
				path: publicPath,
				filename: files.file.name
			})
		})

	})

})

module.exports = router;