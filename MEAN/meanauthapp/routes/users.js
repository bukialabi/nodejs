const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const User = require('../models/user');

//register: implicitly references '/users/register' since we mention that top level './users' in app.js
router.post('/register', (req, res, next) => {
	let newUser = new User({
		name: req.body.name,
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
	});

	User.addUser(newUser, (err, user) => {
		if (err) {
			res.json({
				success: false,
				msg: 'Failed to register user'
			});
		} else {
			res.json({
				success: true,
				msg: 'user registered'
			});
		}
	});
});

router.post('/login', (req, res, next) => {
	const username = req.body.username;
	const password = req.body.password;

	User.getUserByUsername(username, (err, user) => {
		if (err) throw err;
		if (!user) {
			return res.json({
				success: false,
				msg: 'User not found',
			});
		}

		User.comparePassword(password, user.password, (err, isMatch) => {
			if (err) throw err;
			if (isMatch) {
				const token = jwt.sign(user.toJSON(), config.secret, {
					expiresIn: 604800, //1 week in seconds
				});

				res.json({
					success: true,
					token,
					user: {
						id: user._id,
						name: user.name,
						username: user.username,
						email: user.email,
					},
				});
			} else {
				return res.json({
					success: false,
					msg: 'Wrong password',
				});
			}
		});
	});
});

router.get('/me', passport.authenticate('jwt', {
	session: false
}), (req, res, next) => {
	res.json({
		user: req.user
	});
});

router.get('/', (req, res, next) => {
	User.listAll((err, users) => {
		res.json({
			data: users
		})
	})
});

router.post('/picture', (req, res, next) => {
	const username = req.body.username
	const imageUrl = req.body.imageUrl

	User.updatePicture(username, imageUrl, (error, user) => {
		if (error || !user) {
			res.json({
				success: false,
				msg: 'Error updating user picture',
				error
			})
		} else {
			res.json({
				success: true,
				msg: 'Image Updated',
				user: {
					username: user.username
				}
			})
		}

	})
})

module.exports = router;