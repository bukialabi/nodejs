const express = require('express');
const path = require('path'); //A core module, not npm installed
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

mongoose.connect(config.database);
mongoose.connection.on('connected', () => {
	console.log(`connected to ${config.database}`);
});
mongoose.connection.on('error', err => {
	console.error(`error connecting  to ${config.database}`, err);
});

const app = express();

const users = require('./routes/users');
const upload = require('./routes/upload');

const port = config.port;

//Middleware
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);
app.use('/users', users);
app.use('/upload', upload);
app.use(express.static(path.join(__dirname, 'public')));
app.use("/uploads", express.static(path.join(__dirname, 'uploads')));

// Index route
app.get('/', (req, res) => {
	res.send('Invalid Endpoint');
});

// For our angular build, all routes should go through the single page app on index.html
app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname, 'public/index.html'));
});

//Start server
app.listen(port, () => {
	console.log(`Server started on port ${port}`);
});