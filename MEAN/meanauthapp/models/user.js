const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

//User schema
const UserSchema = mongoose.Schema({
	name: {
		type: String
	},
	email: {
		type: String,
		required: true
	},
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	registrationDate: {
		type: Date,
		required: true
	},
	imageUrl: {
		type: String
	}
});

const User = (module.exports = mongoose.model('User', UserSchema));

User.getUserById = function(id, callback) {
	User.findById(id, callback);
};

User.getUserByUsername = function(username, callback) {
	const query = {
		username: username
	};
	User.findOne(query, callback);
};

User.addUser = function(newUser, callback) {
	bcrypt.genSalt(10, (err, salt) => {
		bcrypt.hash(newUser.password, salt, (err, hash) => {
			if (err) throw err;
			newUser.password = hash;
			newUser['registrationDate'] = Date.now()
			newUser.save(callback);
		});
	});
};

User.comparePassword = function(enteredPassword, hash, callback) {
	bcrypt.compare(enteredPassword, hash, (err, isMatch) => {
		if (err) throw err;
		callback(null, isMatch);
	});
};

User.listAll = function(callback) {
	User.find({}, {
		name: 1,
		username: 1,
		email: 1,
		registrationDate: 1,
		imageUrl: 1
	}, callback);
}

User.updatePicture = function(username, imageUrl, callback) {
	const query = {
		username
	}
	const newProperty = {
		imageUrl
	}
	User.findOneAndUpdate(query, newProperty, callback)
}