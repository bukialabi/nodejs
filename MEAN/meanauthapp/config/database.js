const port = 3000;

module.exports = {
	database: process.env.database || 'mongodb://localhost:27017/meanauth',
	secret: 'yoursecret',
	port: process.env.PORT || port,
	baseurl: process.env.baseurl || `http://localhost:${port}`
}