import { Routes } from '@angular/router'

import { AuthService } from './services/auth/auth.service'
import { DirectoryComponent } from './components/directory/directory.component';
import { LoginComponent } from './components/login/login.component'
import { RegisterComponent } from './components/register/register.component'
import { HomeComponent } from './components/home/home.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { ProfileComponent } from './components/profile/profile.component'
import { NotFoundComponent } from './components/not-found/not-found.component'
import { InnerComponent } from './layouts/inner/inner.component'
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component'

export const routes: Routes = [
	{ path: '', component: HomeComponent, pathMatch: 'full' },
	{ path: 'login', redirectTo: '', pathMatch: 'full' },
	{ path: 'register', redirectTo: '', pathMatch: 'full' },
	{
		path: '',
		component: InnerComponent,
		canActivate: [AuthService],
		children: [
			{ path: 'dashboard', component: DashboardComponent },
			{ path: 'profile', component: ProfileComponent },
			{ path: 'directory', component: DirectoryComponent }
		],
	},
	{ path: '404', component: NotFoundComponent },
	{ path: 'unauthorized', component: UnauthorizedComponent },
	{ path: '**', redirectTo: '404' },
]
