import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { HttpInterceptorService } from 'app/services/http-interceptor/http-interceptor.service';
import { InnerComponent } from './layouts/inner/inner.component';
import { JwtModule } from '@auth0/angular-jwt';
import { LoginComponent } from './components/login/login.component';
import { MaterialModule } from './shared/material.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { routes } from './app.routes';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { DirectoryComponent } from './components/directory/directory.component';

export function tokenGetter() {
	return localStorage.getItem('token');
}


@NgModule({
	declarations: [
		AppComponent,
		DashboardComponent,
		HomeComponent,
		InnerComponent,
		LoginComponent,
		ProfileComponent,
		NavbarComponent,
		NotFoundComponent,
		RegisterComponent,
		UnauthorizedComponent,
		DirectoryComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FlexLayoutModule,
		FormsModule,
		HttpClientModule,
		MaterialModule,
		RouterModule.forRoot(routes),
		ReactiveFormsModule,
		SimpleNotificationsModule.forRoot(),
		JwtModule.forRoot({
			config: {
				tokenGetter: tokenGetter,
			}
		})
	],
	providers: [
		{ provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
	],
	bootstrap: [AppComponent],
})
export class AppModule { }
