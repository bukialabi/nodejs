import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  notificationOptions = {
    position: ['top', 'right'],
    timeOut: 6000,
    animate: 'fromRight',
    clickToClose: false,
    showProgressBar: false,
    pauseOnHover: true,
  };
}
