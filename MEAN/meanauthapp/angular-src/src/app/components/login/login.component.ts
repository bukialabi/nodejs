import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthService } from 'app/services/auth/auth.service';
import { NotificationsService } from 'angular2-notifications';
import { UtilityService } from 'app/services/utility/utility.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
	form: FormGroup;

	constructor(
		private formBuilder: FormBuilder,
		private notification: NotificationsService,
		private utility: UtilityService,
		private auth: AuthService,
	) { }

	ngOnInit() {
		this.setForm();
	}

	setForm() {
		this.form = this.formBuilder.group({
			username: [null, [Validators.required]],
			password: [null, [Validators.required]],
		});
	}

	loginBody() {
		return this.utility.formBody(this.form);
	}

	login() {
		if (!this.form.valid) {
			this.notification.error('Your login form has some errors');
			return;
		}
		this.auth.login(this.loginBody())
	}
}
