import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  points = [
    {
      title: 'Express Backend',
      description:
        'A rock solid Node.js/Express server using mongoose to organize models and query the database',
    },
    {
      title: 'Angular-CLI',
      description:
        'Angular-CLI to generate components, services and more. Local dev server and easy compilation',
    },
    {
      title: 'JWT Tokens',
      description: 'Full featured authentication using JSON web tokens. Login and store user data',
    },
  ];

  constructor() {}

  ngOnInit() {}
}
