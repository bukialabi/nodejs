import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';

import { AuthService } from 'app/services/auth/auth.service';
import { NotificationsService } from 'angular2-notifications';
import { UtilityService } from 'app/services/utility/utility.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
	form: FormGroup;
	@ViewChild(FormGroupDirective) myForm;
	@Output() userRegistered = new EventEmitter();

	constructor(
		private formBuilder: FormBuilder,
		private notification: NotificationsService,
		private utility: UtilityService,
		private auth: AuthService,
	) { }

	ngOnInit() {
		this.setForm();
	}

	setForm() {
		this.form = this.formBuilder.group({
			name: [null, [Validators.required]],
			username: [null, [Validators.required]],
			email: [null, [Validators.required]],
			password: [null, [Validators.required]],
		});
	}

	userBody() {
		return this.utility.formBody(this.form);
	}

	register() {
		if (!this.form.valid) {
			this.notification.error('Your registration form has some errors');
			return;
		}
		const user = this.userBody();
		this.auth.register(user).then(res => {
			this.reset();
			this.userRegistered.emit(user);
		})
	}

	/*
     * Source: https://github.com/angular/material2/issues/4190#issuecomment-305222426
     */
	reset() {
		if (this.myForm) {
			this.myForm.resetForm();
		}
	}
}
