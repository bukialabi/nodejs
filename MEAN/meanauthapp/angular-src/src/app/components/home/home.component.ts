import { Component, OnInit } from '@angular/core';

import { NotificationsService } from 'angular2-notifications';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
	selectedIndex = 0;

	constructor(private notification: NotificationsService) { }

	ngOnInit() { }

	welcomeUser(user) {
		this.notification.success(`Welcome ${user.username}`, 'You may now log in');
		this.selectedIndex = 0;
	}
}
