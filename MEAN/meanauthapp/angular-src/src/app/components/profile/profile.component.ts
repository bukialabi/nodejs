import { Component, OnInit } from '@angular/core'
import { UserService } from 'app/services/user/user.service'

import { UploadService } from 'app/services/upload/upload.service'

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	user: Object

	constructor(private userService: UserService, private uploadService: UploadService) { }

	ngOnInit() {
		this.loadProfile()
	}

	loadProfile() {
		this.userService.me().then(profile => {
			this.user = profile['user']
		})
	}

	/*
		Source: https://academind.com/learn/angular/snippets/angular-image-upload-made-easy/
	*/
	uploadFile(event) {
		const file: File = event.target.files[0]
		const formData = new FormData()
		formData.append('file', file, file.name)

		this.uploadService.upload(formData).then(uploadResponse => {
			const body = {
				username: this.user['username'],
				imageUrl: uploadResponse.path
			}
			this.userService.updatePicture(body).then(updateResponse => {
				this.loadProfile()
			})
		})
	}

}
