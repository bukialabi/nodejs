import { Component, OnInit } from '@angular/core'

import { UserService } from 'app/services/user/user.service'

@Component({
	selector: 'app-directory',
	templateUrl: './directory.component.html',
	styleUrls: ['./directory.component.scss']
})
export class DirectoryComponent implements OnInit {
	people

	constructor(private userService: UserService) { }

	ngOnInit() {
		this.loadEveryone()
	}

	loadEveryone() {
		this.userService.everyone().then(people => {
			this.people = people.data
		})
	}

}
