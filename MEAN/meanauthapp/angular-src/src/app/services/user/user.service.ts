import { Injectable } from '@angular/core'
import { HttpClientService } from '../http-client/http-client.service'

import { environment } from 'environments/environment'
import { NotificationsService } from 'angular2-notifications'

@Injectable({
	providedIn: 'root'
})
export class UserService {

	url = `${environment.apiUrl}users`

	constructor(private http: HttpClientService) { }

	me() {
		return this.http.get(`${this.url}/me`)
	}

	everyone() {
		return this.http.get(`${this.url}`)
	}

	updatePicture(body) {
		return this.http.post(`${this.url}/picture`, body)
	}
}
