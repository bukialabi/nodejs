import { Injectable } from '@angular/core'
import { HttpHeaders } from '@angular/common/http'
import { HttpClientService } from '../http-client/http-client.service'
import { Router, CanActivate } from '@angular/router'

import { environment } from 'environments/environment'
import { JwtHelperService } from '@auth0/angular-jwt';
import { NotificationsService } from 'angular2-notifications'

@Injectable({
	providedIn: 'root',
})
export class AuthService implements CanActivate {
	authToken: any
	user: any
	url = `${environment.apiUrl}users`

	constructor(
		private http: HttpClientService,
		private notification: NotificationsService,
		private router: Router,
		private jwt: JwtHelperService) { }

	register(user) {
		return this.http.post(`${this.url}/register`, user)
	}

	login(user) {
		return this.http.post(`${this.url}/login`, user).then(res => {
			if (res.success) this.saveAndGo(res)
			else {
				this.notification.error(`Something's not right`, res.msg)
			}
		})
	}

	saveAndGo(res) {
		localStorage.setItem('token', res.token)
		localStorage.setItem('user', JSON.stringify(res.user))
		this.router.navigate(['/dashboard'])
	}

	getToken() {
		return this.jwt.tokenGetter();
	}

	getUser() {
		const user = localStorage.getItem('token')
		return JSON.parse(user)
	}

	logout() {
		localStorage.clear()
		this.router.navigate(['/'])
	}

	isLoggedIn() {
		return !this.jwt.isTokenExpired()
	}

	canActivate() {
		if (this.isLoggedIn()) {
			return true
		} else {
			this.router.navigate(['/unauthorized'])
			return false
		}
	}
}
