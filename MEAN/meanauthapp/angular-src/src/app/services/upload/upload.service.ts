import { Injectable } from '@angular/core';
import { HttpClientService } from '../http-client/http-client.service'

import { environment } from 'environments/environment'
import { NotificationsService } from 'angular2-notifications'


@Injectable({
	providedIn: 'root'
})
export class UploadService {
	url = `${environment.apiUrl}upload`

	constructor(private http: HttpClientService) { }

	upload(formData) {
		return this.http.post(this.url, formData)
	}
}
