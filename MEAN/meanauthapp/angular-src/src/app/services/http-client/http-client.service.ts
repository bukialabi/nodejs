import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root',
})
export class HttpClientService {
	constructor(private http: HttpClient) { }

	get(url, options?): Promise<any> {
		const req = options ? this.http.get(url, options) : this.http.get(url);
		return req.toPromise().then(res => {
			return this.extractData(res);
		});
	}

	post(url, body, options?): Promise<any> {
		const req = options ? this.http.post(url, body, options) : this.http.post(url, body);
		return req.toPromise().then(res => {
			return this.extractData(res);
		});
	}

	extractData(res) {
		return res;
	}
}
