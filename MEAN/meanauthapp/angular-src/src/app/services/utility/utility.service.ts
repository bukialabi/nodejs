import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
	providedIn: 'root',
})
export class UtilityService {
	constructor() { }

	/**
	 * Parses the control values of a form group to create a flat object
	 * @param form - the form group to be parsed
	 * @returns Object - containing the values of the form controls with control names as keys
	 */
	formBody(form: FormGroup) {
		let entries = Object.entries(form.controls);
		let body = {};
		for (const entry of entries) {
			body[entry[0]] = entry[1].value;
		}
		return body;
	}
}
