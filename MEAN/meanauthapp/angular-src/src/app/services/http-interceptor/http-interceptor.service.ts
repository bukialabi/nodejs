import { Injectable } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor,
	HttpErrorResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class HttpInterceptorService implements HttpInterceptor {
	constructor(
		private auth: AuthService,
		private notification: NotificationsService,
		private router: Router,
	) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const token = this.auth.getToken();

		if (token) {
			req = req.clone({
				headers: req.headers.set('Authorization', `bearer ${token}`),
			});
		}

		return next.handle(req).pipe(
			tap(
				(event: HttpEvent<any>) => { },
				(err: any) => {
					if (err instanceof HttpErrorResponse) {
						this.displayError(err);
						console.warn('Server error');
						//this.goToErrorPage(err.status);
					}
				},
			),
		);
	}

	displayError(err) {
		if (err.error['status'] && err.error['message'])
			this.notification.error(err.error['status'], err.error['message']);
		else
			this.notification.error(err['name'], err['message']);
	}

	//To be used where commented out above when error pages are created
	goToErrorPage(status) {
		switch (status) {
			case 500:
				this.router.navigate(['/error']);
				break;
			case 401:
				this.router.navigate(['/unauthorized']);
				break;
		}
	}
}
